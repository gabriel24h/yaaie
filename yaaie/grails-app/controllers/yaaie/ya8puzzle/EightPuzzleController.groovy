package yaaie.ya8puzzle

import yaaie.ya8puzzle.bestfirstsearch.*
import grails.validation.Validateable
import yaaie.ya8puzzle.heuristics.HeuristicType
import yaaie.ya8puzzle.examples.EightPuzzleExamples
import yaaie.ya8puzzle.examples.EightPuzzleExamplesSelector

class EightPuzzleController {

    def eightPuzzleService

    def index() {
        EightPuzzleCommand command = new EightPuzzleCommand()
        [command: command]
    }

    def program() { }

    def conclusions() { }

    def references() { }

    def solve(EightPuzzleCommand command) {
        command.puzzle = EightPuzzleExamplesSelector.selectExample(command.example)
        command.validate()
        if (command.hasErrors()) {
            render(view: "index", model: [command: command])
            return
        }

        Solution solution
        try {
            solution = eightPuzzleService.solve(command.heuristicType, command.goal, command.puzzle, command.limit, command.downhill)
            if (!solution)
                render(view: "fail")

            render(view: "solution", model: [solution: solution])
        } catch (AStarException ae) {
            Solution failure = new Solution(node: ae.currentNode, goal: ae.goalArray, nodesCreated: ae.nodesCreated, nodesExpanded: ae.nodesExpanded, limit: ae.limit)
            render(view: "fail", model: [currentNode: failure])
        }
    }

    def selectPuzzle() {
        EightPuzzleExamples example = params.example
        int [] puzzle = EightPuzzleExamplesSelector.selectExample(example)
        EightPuzzleCommand command = new EightPuzzleCommand(puzzle: puzzle)
        render(template: "exampleBoard", model: [command: command])
    }
}

@Validateable
class EightPuzzleCommand {
    int [] goal = [1, 2, 3, 8, 0, 4, 7, 6, 5]
    EightPuzzleExamples example = EightPuzzleExamples._1
    int [] puzzle = [2, 8, 3, 1, 6, 4, 7, 0, 5]
    int limit = 100
    boolean downhill = false
    HeuristicType heuristicType = HeuristicType.MISPLACED_TILES

    def static constraints = {
        goal size: 9..9
        puzzle size: 9..9
        limit range: 100..1500
        heuristicType inList: [HeuristicType.MISPLACED_TILES]
    }
}
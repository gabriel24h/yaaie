<%--
  Created by IntelliJ IDEA.
  User: gabriel
  Date: 11/20/12
  Time: 9:04 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Y.A.A.I.E. - Código Fonte</title>
</head>
<body>
<div id="page-body" role="main">
    <h1>Código Fonte - Compilação e Execução</h1>
    <div id="content">
        <div class="text">
            <p>O projeto 8-Puzzle possui a seguinte estrutura:</p>
            <div class="topics" style="width: auto;">
                <ul style="padding: 0;">
                    <li>
                        <p>ya8puzzle/</p>
                        <ul>
                            <li>
                                <p>build.xml</p>
                            </li>
                            <li>
                                <p>src/</p>
                            </li>
                            <li>
                                <p>test/</p>
                            </li>
                            <li>
                                <p>lib/</p>
                            </li>
                            <li>
                                <p>out/</p>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="text">
            <p>O arquivo build.xml contém a descrição para o Ant Build.</p>
            <p>Os fontes estão localizados na pasta /src</p>
            <p>A pasta /test contém arquivos de testes unitários.</p>
        </div>
        <div class="text">
            <p>Na pasta /lib, existe uma biblioteca interessante, Groovy All (groovy-all-2.0.4.jar), que contém os scripts do Groovy JDK.</p>
            <p>Com isso, é possível invocar scripts Groovy durante o Ant Build sem precisar ter o Groovy instalado no computador.</p>
        </div>
        <div class="text">
            <p>A pasta /out é criada durante o Ant Build, e contém os resultados da compilação.</p>
            <p>São gerados os .class a partir dos scripts Groovy em /src, e são importados no classpath durante a execução do script Groovy que contém o método <strong>public static void main(String[] args)</strong></p>
            <p>Se a execução do script foi bem sucedida, o resultado da compilação está correto e então é gerado um JAR executável na pasta /out/artifacts</p>
        </div>
    </div>

    <div class="topics">
        <div id="build">
            <h2>Instruções para Compilação e Execução (Linha de Comando)</h2>
            <hr>
            <g:render template="build-ant"/>
            <hr>
            <g:render template="build-jar"/>
        </div>
    </div>

</div>
</body>
</html>
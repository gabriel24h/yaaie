<div id="spinner" style="position: absolute;
left: 0;
top: 0;
bottom: 0;
right: 0;
/* use any background color you like; rgba would be nice, but older IE will ignore it*/
background: #000;
opacity: 0.8;
filter: alpha(opacity=80);
display: none;">
    <img id="img-spinner" src="${createLinkTo(dir: "images", file: "ajax-loader.gif")}" alt="Loading" style="width: 50px;
    height: 57px;
    position: absolute;
    top: 50%;
    left: 50%;
    /* the image is 50x57, so we use negative margins of 1/2 that size to center it*/
    margin: -28px 0 0 -25px;"/>
    <g:message code="spinner.alt" default="Loading&hellip;"/>
</div>
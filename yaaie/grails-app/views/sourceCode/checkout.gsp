<%--
  Created by IntelliJ IDEA.
  User: gabriel
  Date: 11/20/12
  Time: 9:04 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Y.A.A.I.E. - Código Fonte</title>
</head>
<body>
<div id="page-body" role="main">
    <h1>Código Fonte - Checkout / Download</h1>

    <div id="content">
        <div class="text">
            <p>O código fonte está hospedado no <g:link url="https://bitbucket.org/gabriel24h/yaaie/src" target="_blank">BitBucket</g:link>.</p>
            <p>É possível fazer o <a href="#checkout">checkout</a> do projeto todo, inclusive a aplicação web, sendo necessário ter o <g:link url="http://git-scm.com/" target="_blank">Git</g:link> instalado.</p>
        </div>
        <div class="text">
            <p>Caso prefira, poderá fazer o <a href="#download">download</a> do programa 8-Puzzle isoladamente.</p>

        </div>
        <div class="text">
            <p>Após obter o código fonte, é possível <g:link action="build">compilar e executar</g:link> o projeto.</p>
        </div>
    </div>

    <div class="topics" style="display: none;">
        <div id="checkout">
            <h2>Instruções para Checkout</h2>
            <hr>
            <g:render template="checkout-linux"/>
            <hr>
            <g:render template="checkout-windows"/>
        </div>
    </div>

    <div class="topics">
        <div id="download">
            <h2>Downloads</h2>
            <hr>
            <g:render template="checkout-download"/>
        </div>
    </div>
</div>
</body>
</html>
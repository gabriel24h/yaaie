package yaaie.ya8puzzle.heuristics

import yaaie.ya8puzzle.bestfirstsearch.Node
/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 11/8/12
 * Time: 3:12 AM
 * To change this template use File | Settings | File Templates.
 */
class Heuristic {
    
    public static int calculate(int[] puzzle, int[] goal, HeuristicType heuristicType) {
        switch (heuristicType) {
            case HeuristicType.MISPLACED_TILES:
                return calculateMisplacedTiles(puzzle, goal)
            default:
                return calculateMisplacedTiles(puzzle, goal)
        }
    }

    private static int calculateMisplacedTiles(int[] puzzleArray, int[] goalArray) {

        int misplacedTiles = 0
        for (int i = 0; i < puzzleArray.length; i++) {
            if (puzzleArray[i] != goalArray[i])
                misplacedTiles++
        }
        return misplacedTiles
    }
}

public enum HeuristicType {
    MISPLACED_TILES("MISPLACED_TILES")

    final String value

    HeuristicType(String value) {
        this.value = value
    }

    public String toString() { this.value }

    String getKey() { name() }

}

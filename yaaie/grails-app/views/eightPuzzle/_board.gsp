<g:if test="${node}">
    <div id="node-${node.gCost}" class="node" style="display: none;">
    <ya8puzzle:boardInfo node="${node}"/>
</g:if>
<g:else>
    <div class="draw-puzzle" class="node">
</g:else>
<div class="node-puzzle">
    <table border="1">
        <tr>
            <td class="${ (board[0]) == 0 ? 'blank' : 'number'}">${board[0]}</td>
            <td class="${ (board[1]) == 0 ? 'blank' : 'number'}">${board[1]}</td>
            <td class="${ (board[2]) == 0 ? 'blank' : 'number'}">${board[2]}</td>
        </tr>
        <tr>
            <td class="${ (board[3]) == 0 ? 'blank' : 'number'}">${board[3]}</td>
            <td class="${ (board[4]) == 0 ? 'blank' : 'number'}">${board[4]}</td>
            <td class="${ (board[5]) == 0 ? 'blank' : 'number'}">${board[5]}</td>
        </tr>
        <tr>
            <td class="${ (board[6]) == 0 ? 'blank' : 'number'}">${board[6]}</td>
            <td class="${ (board[7]) == 0 ? 'blank' : 'number'}">${board[7]}</td>
            <td class="${ (board[8]) == 0 ? 'blank' : 'number'}">${board[8]}</td>
        </tr>
    </table>
</div>
</div>
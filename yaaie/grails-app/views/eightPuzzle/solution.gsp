<%--
  Created by IntelliJ IDEA.
  User: gabriel
  Date: 11/9/12
  Time: 1:43 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Y.A.A.I.E. - 8-Puzzle</title>
</head>
<body>
<div id="page-body" role="main">
    <h1>Solução Encontrada!</h1>

    <div class="solution-nav">
        <input type="button" name="prev" id="prev" value="<<"/>
        <input type="button" name="next" id="next" value=">>"/>
    </div>

    <ya8puzzle:solution solution="${solution}"/>

    <g:javascript>
        var index = 0;
        var pathSize = ${solution.pathSize};
        $('#node-' + index).show();
        $('#prev').attr('disabled', true);

        $('#next').click(function() {
            $('#node-' + index).hide();
            index++;
            $('#node-' + index).show();
            updateSolutionNav();
        });

        $('#prev').click(function() {
            $('#node-' + index).hide();
            index--;
            $('#node-' + index).show();
            updateSolutionNav();
        });

        function updateSolutionNav() {
            if (index == 0)
                $('#prev').attr('disabled', true);
            else
                $('#prev').attr('disabled', false);

            if (index == pathSize) {
                $('#next').attr('disabled', true);
            } else {
                $('#next').attr('disabled', false);
            }
        }
    </g:javascript>
</div>
</body>
</html>
package yaaie



import grails.test.mixin.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(SourceCodeController)
class SourceCodeControllerTests {

    void testSomething() {
        fail "Implement me"
    }
}

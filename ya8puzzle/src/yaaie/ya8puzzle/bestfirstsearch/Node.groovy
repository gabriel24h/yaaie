package yaaie.ya8puzzle.bestfirstsearch

import yaaie.ya8puzzle.heuristics.HeuristicType

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 11/8/12
 * Time: 12:19 AM
 * To change this template use File | Settings | File Templates.
 */
class Node implements Comparable<Node> {

    int[] puzzleArray;      //representation of the puzzle tiles
    int hCost;              //represents h(n) in A*
    int gCost = 0;              //represents g(n) in A*
    int pathCost;           //represents f(n) in A*
    Node parentNode;  //parent node of the current node
    Direction direction;       //tells which direction parent node is

    void updatePathCost(){
        this.pathCost = this.hCost + this.gCost;
    }

    int getBlankPosition() {
        return puzzleArray.findIndexOf { it == 0 }
    }

    void printPuzzle() {
        println("""
              | ${puzzleArray[0]} | ${puzzleArray[1]} | ${puzzleArray[2]} |
              | ${puzzleArray[3]} | ${puzzleArray[4]} | ${puzzleArray[5]} |
              | ${puzzleArray[6]} | ${puzzleArray[7]} | ${puzzleArray[8]} |
              """)
    }

    int getPathSize() {
        int size = 0
        Node tempNode = this;
        while (tempNode.parentNode){
            size++
            tempNode = tempNode.parentNode ? tempNode.parentNode : tempNode;
        }
        return size
    }

    void printPath(){
        Node tempNode = this;
        while (tempNode.parentNode){
            print "${tempNode.direction} - ${tempNode.puzzleArray} - Path Cost: ${tempNode.pathCost} <-"
            tempNode.printPuzzle()
            tempNode = tempNode.parentNode ? tempNode.parentNode : tempNode;
        }
        print "START NODE: ${tempNode.puzzleArray} - Path Cost: ${tempNode.pathCost}"
        tempNode.printPuzzle()
        println()
    }

    //Usa custo f(x) de cada nó para atualizar a cabeça da fila de prioridade
    @Override
    int compareTo(Node node) {
        if (this.pathCost > node.pathCost)
            return 1
        else if (this.pathCost < node.pathCost)
            return -1
        else
            return 0
    }
}

public enum Direction {
    UP("UP"),
    DOWN("DOWN"),
    LEFT("LEFT"),
    RIGHT("RIGHT")

    final String value

    Direction(String value) {
        this.value = value
    }

    public String toString() { this.value }

    String getKey() { name() }
}
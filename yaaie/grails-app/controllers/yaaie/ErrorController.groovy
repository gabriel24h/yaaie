package yaaie

class ErrorController {

    def _500() { render(view: "500") }
}

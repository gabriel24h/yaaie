package yaaie.ya8puzzle.examples

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 11/22/12
 * Time: 8:48 PM
 * To change this template use File | Settings | File Templates.
 */
class EightPuzzleExamplesSelector {

    /* Test Cases
    HARD
    12 puzzle = [0, 2, 8, 1, 7, 3, 5, 6, 4]
    11 puzzle = [1, 2, 8, 0, 7, 3, 5, 6, 4]
    10 puzzle = [1, 2, 8, 7, 0, 3, 5, 6, 4]
    9 puzzle = [1, 2, 8, 7, 6, 3, 5, 0, 4]
    8 puzzle = [1, 2, 8, 7, 6, 3, 0, 5, 4]
    7 puzzle = [1, 2, 8, 0, 6, 3, 7, 5, 4]
    6 puzzle = [0, 2, 8, 1, 6, 3, 7, 5, 4]
    5 puzzle = [2, 0, 8, 1, 6, 3, 7, 5, 4]
    4 puzzle = [2, 8, 0, 1, 6, 3, 7, 5, 4]
    3 puzzle = [2, 8, 3, 1, 6, 0, 7, 5, 4]
    2 puzzle = [2, 8, 3, 1, 6, 4, 7, 5, 0]
    1 puzzle = [2, 8, 3, 1, 6, 4, 7, 0, 5]
    EASY
    */

    static int[] selectExample(EightPuzzleExamples example) {
        int [] puzzle
        switch (example) {
            case EightPuzzleExamples._1:
                puzzle = [2, 8, 3, 1, 6, 4, 7, 0, 5]
                return puzzle
            case EightPuzzleExamples._2:
                puzzle = [2, 8, 3, 1, 6, 4, 7, 5, 0]
                return puzzle
            case EightPuzzleExamples._3:
                puzzle = [2, 8, 3, 1, 6, 0, 7, 5, 4]
                return puzzle
            case EightPuzzleExamples._4:
                puzzle = [2, 8, 0, 1, 6, 3, 7, 5, 4]
                return puzzle
            case EightPuzzleExamples._5:
                puzzle = [2, 0, 8, 1, 6, 3, 7, 5, 4]
                return puzzle
            case EightPuzzleExamples._6:
                puzzle = [0, 2, 8, 1, 6, 3, 7, 5, 4]
                return puzzle
            case EightPuzzleExamples._7:
                puzzle = [1, 2, 8, 0, 6, 3, 7, 5, 4]
                return puzzle
            case EightPuzzleExamples._8:
                puzzle = [1, 2, 8, 7, 6, 3, 0, 5, 4]
                return puzzle
            case EightPuzzleExamples._9:
                puzzle = [1, 2, 8, 7, 6, 3, 5, 0, 4]
                return puzzle
            case EightPuzzleExamples._10:
                puzzle = [1, 2, 8, 7, 0, 3, 5, 6, 4]
                return puzzle
            case EightPuzzleExamples._11:
                puzzle = [1, 2, 8, 0, 7, 3, 5, 6, 4]
                return puzzle
            case EightPuzzleExamples._12:
                puzzle = [0, 2, 8, 1, 7, 3, 5, 6, 4]
                return puzzle
            default:
                puzzle = [2, 8, 3, 1, 6, 4, 7, 0, 5]
                return puzzle
        }
    }

    static int[] selectExample(int example) {
        int [] puzzle
        switch (example) {
            case 1:
                puzzle = [2, 8, 3, 1, 6, 4, 7, 0, 5]
                return puzzle
            case 2:
                puzzle = [2, 8, 3, 1, 6, 4, 7, 5, 0]
                return puzzle
            case 3:
                puzzle = [2, 8, 3, 1, 6, 0, 7, 5, 4]
                return puzzle
            case 4:
                puzzle = [2, 8, 0, 1, 6, 3, 7, 5, 4]
                return puzzle
            case 5:
                puzzle = [2, 0, 8, 1, 6, 3, 7, 5, 4]
                return puzzle
            case 6:
                puzzle = [0, 2, 8, 1, 6, 3, 7, 5, 4]
                return puzzle
            case 7:
                puzzle = [1, 2, 8, 0, 6, 3, 7, 5, 4]
                return puzzle
            case 8:
                puzzle = [1, 2, 8, 7, 6, 3, 0, 5, 4]
                return puzzle
            case 9:
                puzzle = [1, 2, 8, 7, 6, 3, 5, 0, 4]
                return puzzle
            case 10:
                puzzle = [1, 2, 8, 7, 0, 3, 5, 6, 4]
                return puzzle
            case 11:
                puzzle = [1, 2, 8, 0, 7, 3, 5, 6, 4]
                return puzzle
            case 12:
                puzzle = [0, 2, 8, 1, 7, 3, 5, 6, 4]
                return puzzle
            default:
                puzzle = [2, 8, 3, 1, 6, 4, 7, 0, 5]
                return puzzle
        }
    }
}

public enum EightPuzzleExamples {
    _1("[2, 8, 3, 1, 6, 4, 7, 0, 5]"),
    _2("[2, 8, 3, 1, 6, 4, 7, 5, 0]"),
    _3("[2, 8, 3, 1, 6, 0, 7, 5, 4]"),
    _4("[2, 8, 0, 1, 6, 3, 7, 5, 4]"),
    _5("[2, 0, 8, 1, 6, 3, 7, 5, 4]"),
    _6("[0, 2, 8, 1, 6, 3, 7, 5, 4]"),
    _7("[1, 2, 8, 0, 6, 3, 7, 5, 4]"),
    _8("[1, 2, 8, 7, 6, 3, 0, 5, 4]"),
    _9("[1, 2, 8, 7, 6, 3, 5, 0, 4]"),
    _10("[1, 2, 8, 7, 0, 3, 5, 6, 4]"),
    _11("[1, 2, 8, 0, 7, 3, 5, 6, 4]"),
    _12("[0, 2, 8, 1, 7, 3, 5, 6, 4]")

    final String value

    EightPuzzleExamples(String value) {
        this.value = value
    }

    public String toString() { this.value }

    String getKey() { name() }
}

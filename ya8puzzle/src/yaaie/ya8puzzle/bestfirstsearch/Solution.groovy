package yaaie.ya8puzzle.bestfirstsearch

import yaaie.ya8puzzle.heuristics.HeuristicType

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 11/22/12
 * Time: 6:24 PM
 * To change this template use File | Settings | File Templates.
 */
class Solution {
    Node node
    int [] goal
    NodeQueue openList
    NodeQueue closedList
    int pathSize
    int nodesCreated
    int nodesExpanded
    int limit
    boolean downhill
    HeuristicType heuristicType
}

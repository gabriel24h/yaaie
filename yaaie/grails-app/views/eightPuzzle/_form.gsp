<%@ page import="yaaie.ya8puzzle.examples.EightPuzzleExamples; yaaie.ya8puzzle.heuristics.HeuristicType" %>
<div id="form-container">
    <fieldset>
        <g:hasErrors bean="${command}">
            <div class="errors">
                <g:renderErrors bean="${command}"/>
            </div>
        </g:hasErrors>

        <g:form controller="eightPuzzle" action="solve">

            <label for="puzzle"><strong>Situação Inicial:</strong></label>
            <div id="puzzle">
                <g:select id="example" name="example" from="${EightPuzzleExamples}" optionKey="key" onchange="${remoteFunction(controller: "eightPuzzle", action: "selectPuzzle", params: "\'example=\'+ this.value", update: "example-board")}"/>
                <br>
                <br>
                <div id="example-board">
                    <g:render template="exampleBoard" model="[command: command]"/>
                </div>

            </div>

            <br>

            <label for="goal"><strong>Situação Objetivo:</strong></label>
            <div id="goal">
                <ya8puzzle:drawBoard puzzle="${command?.goal}"/>
                <g:hiddenField name="goal" value="${command?.goal}"/>
            </div>

            <br>

            <label for="heuristicType"><strong>Heurística:</strong></label>
            <g:select name="heuristicType" from="${HeuristicType.MISPLACED_TILES}" optionKey="key" valueMessagePrefix="heuristic.type" disabled="disabled"/>

            <br>

            <label for="downhill"><strong>Estritamente Decrescente (Downhill):</strong></label>
            <g:checkBox name="downhill" value="${command?.downhill}"/>

            <br>

            <label for="limit"><strong>Limite Iterações (número de 100 a 1500):</strong></label>
            <g:textField name="limit" value="${command?.limit}"/>

            <br>

            <g:submitButton name="submit" value="Solucionar!"/>
        </g:form>
    </fieldset>
</div>

<g:javascript>
function updateExampleBoard() {
        var example = $("#exmaple").val();
        alert(example);
        ${remoteFunction(controller: "eightPuzzle", action: "selectPuzzle", params: "'example='+example", update: "example-board")}
    }
</g:javascript>
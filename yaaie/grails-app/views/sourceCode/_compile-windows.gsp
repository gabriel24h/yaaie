<div class="compile-windows">
    <h4>Windows</h4>
    <ul>
        <li>
            <p><g:link url="http://www.oracle.com/technetwork/java/javase/downloads/index.html">Download</g:link> JDK versão 1.6+</p>
        </li>
        <li>
            <p><g:link url="http://ant.apache.org/bindownload.cgi" target="_blank">Download</g:link> Apache Ant</p>
        </li>
        <li>
            <p>Execute o instalador do JDK. Atenção para o path aonde irá instalar.</p>
        </li>
        <li>
            <p>Adicionar JDK no Path</p>
            <ul>
                <li>
                    <p>Menu Iniciar -> Painel de Controle -> Sistema -> Configurações Avançadas do Sistema</p>
                    <p>Selecione a aba Avançado e clique em Variáveis de Ambiente</p>
                </li>
                <li>
                    <p>Em Variáveis do Sistema, clique em Novo</p>
                    <ul>
                        <li>
                            <p>No campo Nome da Variável, digite JAVA_HOME</p>
                            <p>No campo Valor da Variável, digite o path de instalação do JDK. Exemplo:</p>
                            <p><strong>C:\Program Files\Java\jdk1.7.0_09</strong></p>
                        </li>
                    </ul>
                    <p>Em Variáveis do Sistema, selecione a variável Path e clique em editar</p>
                    <ul>
                        <li>
                            <p>No campo Valor da Variável, após a última declaração insira o JAVA_HOME. Exemplo:</p>
                            <p><strong>%SystemRoot%\system32;&ltoutras declarações&gt;%JAVA_HOME%\bin</strong></p>

                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <p>Teste a instalação do JDK</p>
            <p>Menu Iniciar -> Executar -> cmd</p>
            <p><strong>$ java -version</strong></p>
            <p>Se o comando não for encontrado, verifique novamente o classpath e os caminhos de diretório da instalação do JDK.</p>
            <p>Se o erro persistir, pode ser necessário reiniciar o computador.</p>
        </li>
        <li>
            <p>A instalação do Apache Ant é análoga.</p>
            <p>Descompacte o arquivo para diretório de sua preferência, por exemplo C:\ant e siga os passos para adicionar ao path, utilizando a variável de ambiente ANT_HOME</p>
        </li>
        <li>
            <p>Teste a instalação do Apache Ant</p>
            <p>Menu Iniciar -> Executar -> cmd</p>
            <p><strong>$ ant -version</strong></p>
            <p>Se o comando não for encontrado, verifique novamente o classpath e os caminhos de diretório da instalação do Apache Ant.</p>
            <p>Se o erro persistir, pode ser necessário reiniciar o computador.</p>
        </li>
    </ul>
</div>
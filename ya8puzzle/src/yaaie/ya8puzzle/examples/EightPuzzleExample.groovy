package yaaie.ya8puzzle.examples

import yaaie.ya8puzzle.heuristics.HeuristicType
import yaaie.ya8puzzle.bestfirstsearch.AStar
import yaaie.ya8puzzle.bestfirstsearch.Node
import yaaie.ya8puzzle.bestfirstsearch.AStarException

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 11/20/12
 * Time: 12:51 PM
 * To change this template use File | Settings | File Templates.
 */
class EightPuzzleExample {

    /* Test Cases
    HARD
    puzzle = [0, 2, 8, 1, 7, 3, 5, 6, 4]
    puzzle = [1, 2, 8, 0, 7, 3, 5, 6, 4]
    puzzle = [1, 2, 8, 7, 0, 3, 5, 6, 4]
    puzzle = [1, 2, 8, 7, 6, 3, 5, 0, 4]
    puzzle = [1, 2, 8, 7, 6, 3, 0, 5, 4]
    puzzle = [1, 2, 8, 0, 6, 3, 7, 5, 4]
    puzzle = [0, 2, 8, 1, 6, 3, 7, 5, 4]
    puzzle = [2, 0, 8, 1, 6, 3, 7, 5, 4]
    puzzle = [2, 8, 0, 1, 6, 3, 7, 5, 4]
    puzzle = [2, 8, 3, 1, 6, 0, 7, 5, 4]
    puzzle = [2, 8, 3, 1, 6, 4, 7, 5, 0]
    puzzle = [2, 8, 3, 1, 6, 4, 7, 0, 5]
    EASY
    */

    public static void main(String[] args) {
        HeuristicType heuristicType = HeuristicType.MISPLACED_TILES
        int[] goal = [1, 2, 3, 8, 0, 4, 7, 6, 5]
        int[] puzzle = [2, 8, 3, 1, 6, 4, 7, 0, 5] //choose one of the examples above and build with Apache Ant
        int limit = 1000
        int example = 1
        boolean downhill = false

        /* args setUp */
        try {
            if (args) {
                for (int i = 0; i < args.length; i++) {
                    switch(args[i]) {
                        case '-d':
                            downhill = true
                            break
                        case '-l':
                            limit = Integer.parseInt(args[i+1])
                            if (limit < 1)
                                throw new NumberFormatException()
                            break
                        case '-e':
                            example = Integer.parseInt(args[i+1])
                            puzzle = EightPuzzleExamplesSelector.selectExample(example)
                            break
                    }
                }
            }
        } catch(ArrayIndexOutOfBoundsException e) {
            println 'Arg value not specified. Usage example: -l 100 -e 2'
            return
        } catch(NumberFormatException e) {
            println 'Invalid number format specified. Usage example: -l 100 -e 2'
            return
        }
        /* end args setUp */

        AStar solver = new AStar(goal)
        Node startNode = new Node(puzzleArray: puzzle, gCost: 0, hCost: 5, pathCost: 5)
        try {
            solver.solve(heuristicType, startNode, limit, downhill)
        } catch (AStarException ae) {
            println ae.message
            println("Current Node Partial Path:")
            println()
            ae.currentNode.printPath()
        }
    }
}

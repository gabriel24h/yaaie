package yaaie.ya8puzzle

import yaaie.ya8puzzle.bestfirstsearch.*

class EightPuzzleTagLib {

    static namespace = "ya8puzzle"

    def form = { attrs, body ->
        EightPuzzleCommand command = attrs.commmand
        out << render(template: "/eightPuzzle/form", model: [command: command])
    }

    def formErrors = { attrs, body ->
        EightPuzzleCommand command = attrs.commmand
        out << render(template: "/common/form-errors", model: [command: command])
    }

    def solution = { attrs, body ->
        Solution solution = attrs.solution
        Node node = solution.node

        while (node.parentNode) {
            out << render(template: "/eightPuzzle/board", model: [board: node.puzzleArray, node: node])
            node = node.parentNode ? node.parentNode : node
        }
        out << render(template: "/eightPuzzle/board", model: [board: node.puzzleArray, node: node])

        out << render(template: "/eightPuzzle/solutionInfo", model: [solution: solution])
    }

    def boardInfo = { attrs, body ->
        Node node = attrs.node
        out << render(template: "/eightPuzzle/boardInfo", model: [node: node])
    }

    def drawBoard = { attrs, body ->
        def puzzle = attrs.puzzle
        Node node = attrs?.node
        out << render(template: "/eightPuzzle/board", model: [board: puzzle, node: node])
    }

    def drawSolutionInfoBoard = { attrs, body ->
        def puzzle = attrs.puzzle
        Node node = attrs?.node
        out << render(template: "/eightPuzzle/solutionInfoBoard", model: [board: puzzle, node: node])
    }

}

<div class="run-windows">
    <h4>Windows</h4>
    <ul>
        <li>
            <p><g:link url="http://www.oracle.com/technetwork/java/javase/downloads/index.html">Download</g:link> JRE versão 1.6+</p>
        </li>
        <li>
            <p>Execute o instalador do JRE. Atenção para o path aonde irá instalar.</p>
        </li>
        <li>
            <p>Adicionar JRE no Path</p>
            <ul>
                <li>
                    <p>Menu Iniciar -> Painel de Controle -> Sistema -> Configurações Avançadas do Sistema</p>
                    <p>Selecione a aba Avançado e clique em Variáveis de Ambiente</p>
                </li>
                <li>
                    <p>Em Variáveis do Sistema, clique em Novo</p>
                    <ul>
                        <li>
                            <p>No campo Nome da Variável, digite JAVA_HOME</p>
                            <p>No campo Valor da Variável, digite o path de instalação do JRE. Exemplo:</p>
                            <p><strong>C:\Program Files\Java\jdk1.7.0_09</strong></p>
                        </li>
                    </ul>
                    <p>Em Variáveis do Sistema, selecione a variável Path e clique em editar</p>
                    <ul>
                        <li>
                            <p>No campo Valor da Variável, após a última declaração insira o JAVA_HOME. Exemplo:</p>
                            <p><strong>%SystemRoot%\system32;&ltoutras declarações&gt;%JAVA_HOME%\bin</strong></p>

                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <p>Teste a instalação do JRE</p>
            <p>Menu Iniciar -> Executar -> cmd</p>
            <p><strong>$ java -version</strong></p>
            <p>Se o comando não for encontrado, verifique novamente o classpath e os caminhos de diretório da instalação do JRE.</p>
            <p>Se o erro persistir, pode ser necessário reiniciar o computador.</p>
        </li>
    </ul>
</div>
package yaaie.ya8puzzle.bestfirstsearch

import org.junit.Test

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 11/8/12
 * Time: 9:47 PM
 * To change this template use File | Settings | File Templates.
 */
class AStarTests extends GroovyTestCase {

    @Test
    void testGoalSuccess() {
        int[] goal = [0, 1, 2, 3, 4, 5, 6, 7, 8]
        AStar solver = new AStar(goal)

        Node solutionNode = new Node(puzzleArray: [0, 1, 2, 3, 4, 5, 6, 7, 8])

        assert Arrays.equals(solver.goalArray, solutionNode.puzzleArray)
    }

    @Test
    void testGoalFail() {
        int[] goal = [0, 1, 2, 3, 4, 5, 6, 7, 8]
        AStar solver = new AStar(goal)

        Node solutionNode = new Node(puzzleArray: [1, 0, 2, 3, 4, 5, 6, 7, 8])

        assert !(Arrays.equals(solver.goalArray, solutionNode.puzzleArray))
    }
}

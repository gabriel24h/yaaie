package yaaie.ya8puzzle

import grails.test.GrailsUnitTestCase
import org.junit.Test

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 11/8/12
 * Time: 5:06 PM
 * To change this template use File | Settings | File Templates.
 */
class EightPuzzleServiceIntegrationTests extends GrailsUnitTestCase {

    static transactional = false
    def eightPuzzleService

    @Test
    void testSolve() {
        assertNotNull eightPuzzleService.solve()
    }


}

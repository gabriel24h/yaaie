package yaaie.ya8puzzle.bestfirstsearch

import org.junit.Test

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 11/8/12
 * Time: 11:19 PM
 * To change this template use File | Settings | File Templates.
 */
class NodeTests extends GroovyTestCase {

    @Test
    void testPrintPuzzle() {
        Node node = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 7, 0, 5])
        node.printPuzzle()
    }

    @Test
    void testGetBlankPosition() {
        Node node = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 7, 0, 5])
        assert node.getBlankPosition() == 7
    }
}

<div class="build-ant">
    <h4>Apache Ant Build</h4>
    <ul>
        <li>
            <p>Descompacte o arquivo ya8puzzle-1.0.zip no local de sua preferência.</p>
        </li>
        <li>
            <p>Navegue até a raíz do projeto, onde existe o arquivo build.xml</p>
        </li>
        <li>
            <p>Execute o comando ant</p>
            <p><strong>$ ant</strong></p>
        </li>
    </ul>
</div>
<div class="compile-linux">
    <h4>Linux (Ubuntu)</h4>
    <ul>
        <li>
            <p><g:link url="http://www.oracle.com/technetwork/java/javase/downloads/index.html">Download</g:link> JDK versão 1.6+</p>
        </li>
        <li>
            <p>Download Apache Ant</p>
            <p><strong>$ sudo apt-get install ant</strong></p>
        </li>
        <li>
            <p>Extraia o arquivo do JDK para o local de sua preferência. Exemplo:</p>
            <p><strong>$ mv Downloads/jdk-7u&ltversão&gt-linux-x64.tar.gz development/</strong></p>
            <p><strong>$ cd development</strong></p>
            <p><strong>$ tar zxvf jdk-7u&ltversão&gt-linux-x64.tar.gz</strong></p>
        </li>
        <li>
            <p>Adicionar JDK no Classpath</p>
            <ul>
                <li>
                    <p>Edite o arquivo /etc/profile (~/.bashrc em versões antigas) com o editor de texto de sua preferência.</p>
                    <p><strong>$ sudo nano /etc/profile</strong></p>
                </li>
                <li>
                    <p>Adicione as seguintes linhas no fim do arquivo:</p>
                    <p>JAVA_HOME=/&ltpath aonde foi extraido o JDK&gt/jdk&ltversão&gt</p>
                    <p>PATH=$JAVA_HOME/bin:$PATH export PATH JAVA_HOME</p>
                    <p>CLASSPATH=$JAVA_HOME/lib/tools.jar</p>
                    <p>CLASSPATH=.:$CLASSPATH</p>
                    <p>export PATH JAVA_HOME CLASSPATH</p>
                </li>
                <li>
                    <p>Salve o arquivo e feche o editor de texto.</p>
                </li>
            </ul>
        </li>
        <li>
            <p>Teste a instalação do JDK</p>
            <p><strong>$ java -version</strong></p>
            <p>Se o comando não for encontrado, verifique novamente o classpath e os caminhos de diretório da instalação do JDK.</p>
            <p>Se o erro persistir, pode ser necessário reiniciar o computador.</p>
        </li>
        <li>
            <p>Teste a instalação do Apache Ant</p>
            <p><strong>$ ant -version</strong></p>
            <p>Se o comando não for encontrado, é possível <g:link url="http://ant.apache.org/bindownload.cgi" target="_blank">instalar manualmente</g:link>.</p>
            <p>O processo é análogo à instalação do JDK, com a diferença no nome da variável de ambiente que deverá ser ANT_HOME.</p>
        </li>
    </ul>
</div>
package yaaie

class SourceCodeController {

    def index() { }

    def requirements() { }

    def checkout() { }

    def build() { }
}

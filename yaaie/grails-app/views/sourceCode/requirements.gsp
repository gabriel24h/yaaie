<%--
  Created by IntelliJ IDEA.
  User: gabriel
  Date: 11/20/12
  Time: 9:04 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Y.A.A.I.E. - Código Fonte</title>
</head>
<body>
<div id="page-body" role="main">
    <h1>Código Fonte - Requerimentos</h1>

    <div id="content">
        <div class="text">
            <p>Para <a href="#compile">compilar</a> o programa 8-Puzzle, é necessário instalar o Java Development Kit (JDK) e o Apache Ant.</p>
            <p>Caso queira apenas <a href="#run">executar</a>, é necessário ter instalado o Java Runtime Environment (JRE).</p>
        </div>
        <div class="text">
            <p>Após antender os requerimentos, terá que fazer o <g:link action="checkout">checkout ou download</g:link> do projeto.</p>
        </div>
    </div>

    <div class="topics">
        <div id="compile">
            <h2>Requerimentos para Compilar</h2>
            <hr>
            <g:render template="compile-linux"/>
            <hr>
            <g:render template="compile-windows"/>
        </div>
    </div>

    <div class="topics">
        <div id="run">
            <h2>Requerimentos para Executar</h2>
            <hr>
            <g:render template="run-linux"/>
            <hr>
            <g:render template="run-windows"/>
        </div>
    </div>

</div>
</body>
</html>
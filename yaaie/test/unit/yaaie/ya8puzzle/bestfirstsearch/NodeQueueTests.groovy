package yaaie.ya8puzzle.bestfirstsearch

import org.junit.Test

import static java.util.Arrays.equals

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 11/8/12
 * Time: 7:16 PM
 * To change this template use File | Settings | File Templates.
 */
class NodeQueueTests extends GroovyTestCase {

    @Test
    void testContainsSuccess() {
        NodeQueue closedList = new NodeQueue()
        Node initialState = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 7, 0, 5], gCost: 0, hCost: 9, pathCost: 9)
        closedList.add(initialState)
        Node _1stExpandedChild = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 0, 7, 5], gCost: 1, hCost: 8, pathCost: 9, direction: Direction.LEFT, parentNode: initialState)
        Node generatedChild = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 7, 0, 5], gCost: 2, hCost: 9, pathCost: 11, direction: Direction.RIGHT, parentNode: _1stExpandedChild)

        assert closedList.puzzleArray.toString().contains(generatedChild.puzzleArray.toString())
    }

    @Test
    void testContainsFail() {
        NodeQueue closedList = new NodeQueue()
        Node initialState = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 7, 0, 5], gCost: 0, hCost: 9, pathCost: 9)
        closedList.add(initialState)
        Node _1stExpandedChild = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 0, 7, 5], gCost: 1, hCost: 8, pathCost: 9, direction: Direction.LEFT, parentNode: initialState)
        Node generatedChild = new Node(puzzleArray: [2, 8, 3, 0, 6, 4, 1, 7, 5], gCost: 2, hCost: 8, pathCost: 10, direction: Direction.UP, parentNode: _1stExpandedChild)

        assert !(closedList.puzzleArray.toString().contains(generatedChild.puzzleArray.toString()))
    }

    @Test
    void testContainsNodeSuccess() {
        NodeQueue closedList = new NodeQueue()
        Node initialState = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 7, 0, 5], gCost: 0, hCost: 9, pathCost: 9)
        closedList.add(initialState)
        Node _1stExpandedChild = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 0, 7, 5], gCost: 1, hCost: 8, pathCost: 9, direction: Direction.LEFT, parentNode: initialState)
        Node generatedChild = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 7, 0, 5], gCost: 2, hCost: 9, pathCost: 11, direction: Direction.RIGHT, parentNode: _1stExpandedChild)

        assert closedList.containsNode(generatedChild)
    }

    @Test
    void testContainsNodeFail() {
        NodeQueue closedList = new NodeQueue()
        Node initialState = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 7, 0, 5], gCost: 0, hCost: 9, pathCost: 9)
        closedList.add(initialState)
        Node _1stExpandedChild = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 0, 7, 5], gCost: 1, hCost: 8, pathCost: 9, direction: Direction.LEFT, parentNode: initialState)
        Node generatedChild = new Node(puzzleArray: [2, 8, 3, 0, 6, 4, 1, 7, 5], gCost: 2, hCost: 8, pathCost: 10, direction: Direction.UP, parentNode: _1stExpandedChild)

        assert !closedList.containsNode(generatedChild)
    }

    @Test
    void testFind() {
        NodeQueue closedList = new NodeQueue()
        Node initialState = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 7, 0, 5], gCost: 0, hCost: 9, pathCost: 9)
        closedList.add(initialState)
        Node _1stExpandedChild = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 0, 7, 5], gCost: 1, hCost: 8, pathCost: 9, direction: Direction.LEFT, parentNode: initialState)
        Node generatedChild = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 7, 0, 5], gCost: 2, hCost: 9, pathCost: 11, direction: Direction.RIGHT, parentNode: _1stExpandedChild)

        Node inClosedListNode = closedList.find { equals(it.puzzleArray, generatedChild.puzzleArray) }
        assert inClosedListNode
        assert generatedChild.pathCost > inClosedListNode.pathCost
    }

    @Test
    void testFindNodeSuccess() {
        NodeQueue closedList = new NodeQueue()
        Node initialState = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 7, 0, 5], gCost: 0, hCost: 9, pathCost: 9)
        closedList.add(initialState)
        Node _1stExpandedChild = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 0, 7, 5], gCost: 1, hCost: 8, pathCost: 9, direction: Direction.LEFT, parentNode: initialState)
        Node generatedChild = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 7, 0, 5], gCost: 2, hCost: 9, pathCost: 11, direction: Direction.RIGHT, parentNode: _1stExpandedChild)

        Node inClosedListNode = closedList.findNode(generatedChild)
        assert inClosedListNode
        assert generatedChild.pathCost > inClosedListNode.pathCost
    }

    @Test
    void testFindNodeFail() {
        NodeQueue closedList = new NodeQueue()
        Node initialState = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 7, 0, 5], gCost: 0, hCost: 9, pathCost: 9)
        closedList.add(initialState)
        Node _1stExpandedChild = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 0, 7, 5], gCost: 1, hCost: 8, pathCost: 9, direction: Direction.LEFT, parentNode: initialState)
        Node generatedChild = new Node(puzzleArray: [2, 8, 3, 1, 6, 4, 7, 0, 5], gCost: 2, hCost: 9, pathCost: 11, direction: Direction.RIGHT, parentNode: _1stExpandedChild)

        Node inClosedListNode = closedList.findNode(_1stExpandedChild)
        assert !inClosedListNode
    }
}
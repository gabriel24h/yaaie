package yaaie.ya8puzzle

import yaaie.ya8puzzle.bestfirstsearch.*
import yaaie.ya8puzzle.heuristics.*

class EightPuzzleService {

    Solution solve(HeuristicType heuristicType = HeuristicType.MISPLACED_TILES, int[] goal = [1, 2, 3, 8, 0, 4, 7, 6, 5], int[] puzzle = [2, 8, 3, 1, 6, 4, 7, 0, 5], int limit = 1000, boolean downhill = false) {
        AStar solver = new AStar(goal)
        Node startNode = createNode(puzzle, goal)

        return solver.solve(heuristicType, startNode, limit, downhill)
    }

    Node createNode(int[] puzzle, int[] goal, Direction direction = null, Node parentNode = null, HeuristicType heuristicType = HeuristicType.MISPLACED_TILES) {
        Node node = new Node(puzzleArray: puzzle, parentNode: parentNode, direction: direction)
        node.gCost = parentNode ? parentNode.gCost + 1 : 0
        node.hCost = Heuristic.calculate(puzzle, goal, heuristicType)
        node.updatePathCost()
        return node
    }

}

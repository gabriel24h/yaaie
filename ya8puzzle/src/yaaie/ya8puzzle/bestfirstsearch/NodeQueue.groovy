package yaaie.ya8puzzle.bestfirstsearch

import static java.util.Arrays.equals

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 11/8/12
 * Time: 3:29 AM
 * To change this template use File | Settings | File Templates.
 */

/*  Java SDK: Class PriorityQueue(int initialCapacity, Comparator<? super E> comparator)
    *       método poll() - atualiza a fila e retorna a cabeça
    *
    Java SDK: Interface Comparator<T>
    *       implementa critério de comparação compare(Obj o1, Obj o2)
*/
class NodeQueue extends PriorityQueue<Node> implements Comparator<Node> {

    //Usa custo f(x) de cada nó para atualizar a cabeça da fila de prioridade
    @Override
    public int compare(Node a, Node b) {
        if (a.pathCost > b.pathCost)
            return 1
        else if (a.pathCost < b.pathCost)
            return -1
        else
            return 0
    }

    boolean containsNode(Node node) {
        return this.puzzleArray.toString().contains(node.puzzleArray.toString())
    }

    Node findNode(Node node) {
        return this.find { equals(it.puzzleArray, node.puzzleArray) }
    }
}

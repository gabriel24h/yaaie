package yaaie.ya8puzzle.bestfirstsearch

import yaaie.ya8puzzle.heuristics.*
import static java.util.Arrays.*

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 11/8/12
 * Time: 12:19 AM
 * To change this template use File | Settings | File Templates.
 */
class AStarException extends RuntimeException {
    String message
    Node currentNode
    int nodesExpanded
    int nodesCreated
    int[] goalArray
    int limit

    String toString() {
        return  """$message
                Current Node: ${currentNode.puzzleArray}
                Nodes Expanded: $nodesExpanded
                Nodes Created: $nodesCreated
                Goal Array: $goalArray"""
    }
}

class AStar {

    final int[] goalArray
    int nodesExpanded = 0
    int nodesCreated = 0
    NodeQueue openList = new NodeQueue()
    NodeQueue closedList = new NodeQueue()

    AStar(int[] goal) {
        goalArray = goal
    }

    /*
     * translates pseudocode for the A* algorithm into programmatic form.
     * Algorithm used is a combination of those found in the book and on Wikipedia.
     */
    Solution solve(HeuristicType heuristicType, Node startNode, int limit = 1000, boolean downhill = false) {
        /* setUp */
        if (!goalArray)
            throw new AStarException(message: "Set Up Error: goal not informed", nodesCreated: nodesCreated, nodesExpanded: nodesExpanded, goalArray: goalArray, limit: limit)

        nodesExpanded = 0
        nodesCreated = 1 // raíz startNode

        Node currentNode
        closedList = new NodeQueue()
        openList = new NodeQueue()
        openList.add(startNode)

        /* end setUp */

        while (!openList.isEmpty()) {
            currentNode = openList.poll()
            if (downhill)
                openList = new NodeQueue()

            if (nodesExpanded == limit) {
                throw new AStarException(message: "Failure: goal not found in ${limit} iterations.", currentNode: currentNode, nodesCreated: nodesCreated, nodesExpanded: nodesExpanded, goalArray: goalArray, limit: limit)
            }

            if (equals(currentNode.puzzleArray, goalArray)) {
                println("""
                        Solution Found!!!
                            Total Nodes Expanded: $nodesExpanded
                            Solution Path Size: ${currentNode.getPathSize()}
                        """)
                currentNode.printPath()
                return new Solution(node: currentNode, goal: goalArray, openList: openList, closedList: closedList, pathSize: currentNode.pathSize, nodesCreated: nodesCreated, nodesExpanded: nodesExpanded, limit: limit, downhill: downhill, heuristicType: heuristicType)
            } else {
                for (child in generateChildren(currentNode, heuristicType)) {
                    if (!(openList.containsNode(child)) || !(closedList.containsNode(child))) {
                        openList.add(child)
                    }
                    if (openList.containsNode(child)) {
                        Node inOpenListNode = openList.findNode(child)
                        if (child.pathCost < inOpenListNode.pathCost) {
                            openList.remove(inOpenListNode)
                            openList.add(child)
                        }
                    }
                    if (closedList.containsNode(child)) {
                        Node inClosedListNode = closedList.findNode(child)
                        if (child.pathCost < inClosedListNode.pathCost) {
                            closedList.remove(inClosedListNode)
                            openList.add(child)
                        }
                    }
                }
                closedList.add(currentNode)
            }
        }
    }

    NodeQueue generateChildren(Node node, HeuristicType heuristicType) {
        NodeQueue nodeQueue = new NodeQueue()
        int[] swapArray
        final int[] staticArray = copyOf(node.puzzleArray, node.puzzleArray.length)
        int position = getOpenPosition(node.puzzleArray)
        /*
       Baseado na posição da peça em branco, gera os filhos do nó atual em nodeQueue

           0 1 2
           3 4 5
           6 7 8
        */
        switch (position) {
            case 0:
                // update the node info, add node as a parent, and add to the tempQueue
                swapArray = positionSwap(position, 1, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.RIGHT, heuristicType))
                // update for 2nd possible position (from 0)
                swapArray = positionSwap(position, 3, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.DOWN, heuristicType))
                break
            case 1:
                swapArray = positionSwap(position, 0, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.LEFT, heuristicType))

                swapArray = positionSwap(position, 2, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.RIGHT, heuristicType))

                swapArray = positionSwap(position, 4, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.DOWN, heuristicType))
                break
            case 2:
                swapArray = positionSwap(position, 1, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.LEFT, heuristicType))

                swapArray = positionSwap(position, 5, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.DOWN, heuristicType))
                break
            case 3:
                swapArray = positionSwap(position, 0, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.UP, heuristicType))

                swapArray = positionSwap(position, 4, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.RIGHT, heuristicType))

                swapArray = positionSwap(position, 6, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.DOWN, heuristicType))
                break
            case 4:
                swapArray = positionSwap(position, 1, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.UP, heuristicType))

                swapArray = positionSwap(position, 3, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.LEFT, heuristicType))

                swapArray = positionSwap(position, 5, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.RIGHT, heuristicType))

                swapArray = positionSwap(position, 7, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.DOWN, heuristicType))
                break
            case 5:
                swapArray = positionSwap(position, 2, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.UP, heuristicType))

                swapArray = positionSwap(position, 4, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.LEFT, heuristicType))

                swapArray = positionSwap(position, 8, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.DOWN, heuristicType))
                break
            case 6:
                swapArray = positionSwap(position, 3, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.UP, heuristicType))

                swapArray = positionSwap(position, 7, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.RIGHT, heuristicType))
                break
            case 7:
                swapArray = positionSwap(position, 4, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.UP, heuristicType))

                swapArray = positionSwap(position, 6, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.LEFT, heuristicType))

                swapArray = positionSwap(position, 8, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.RIGHT, heuristicType))
                break
            case 8:
                swapArray = positionSwap(position, 5, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.UP, heuristicType))

                swapArray = positionSwap(position, 7, staticArray)
                nodeQueue.add(createSuccessorNode(swapArray, node, Direction.LEFT, heuristicType))
                break
            default:
                throw new AStarException(message: "Error positionSwap(): blank position not in range 0..8", currentNode: node, nodesCreated: nodesCreated, nodesExpanded: nodesExpanded, goalArray: goalArray)
        }

        nodesExpanded++
        println ("""

                    -------ITERATION $nodesExpanded-------


                    --- A* Info ---
                    Nodes Expanded: $nodesExpanded
                    Nodes Created: $nodesCreated
                    Open List: ${openList.puzzleArray}
                        size: ${openList.size()}
                    Closed List: ${closedList.puzzleArray}
                        size: ${closedList.size()}
                    --------------------------------------


                    --- Current Node Info ---
                    State: ${node.puzzleArray}
                    Children: ${nodeQueue.puzzleArray }
                    gCost: ${node.gCost}
                    hCost: ${node.hCost}
                    Path Cost: ${node.pathCost}
                    Direction: ${node.direction}
                    Blank Position: $position
                    Partial Path:
                """)
        node.printPath()

        return nodeQueue
    }

    int getOpenPosition(int[] puzzleArray) {
        int position = 0
        for (int i = 0; i < puzzleArray.length; i++) {
            if (puzzleArray[i] == 0)
                position = i
        }
        return position
    }


    /*
    * swaps the position of the 0 and another integer within an array and returns the array.
    */
    int[] positionSwap(int blank, int swapInt, int[] array) {
        int[] tmp = copyOf(array, array.length)
        tmp[blank] = tmp[swapInt]
        tmp[swapInt] = 0
        return copyOf(tmp, tmp.length)
    }

    /* takes the puzzle array, the parentNode, and the direction of the move in
   / and creates a successor node with an updated gCost, and updated pathCost.
   / Also iterates the number of nodes created.
    */

    Node createSuccessorNode(int[] puzzle, Node parentNode, Direction direction, HeuristicType heuristicType) {
        Node node = new Node(puzzleArray: puzzle, parentNode: parentNode, direction: direction)
        node.gCost = parentNode.gCost + 1
        node.hCost = Heuristic.calculate(puzzle, goalArray, heuristicType)
        node.updatePathCost()

        nodesCreated++

        return node
    }

}
package yaaie.ya8puzzle

import yaaie.ya8puzzle.*
import grails.test.GrailsUnitTestCase
import org.junit.Test

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 11/8/12
 * Time: 5:06 PM
 * To change this template use File | Settings | File Templates.
 */
class EightPuzzleIntegrationTests extends GrailsUnitTestCase {

    @Test
    void testSolve() {
	HeuristicType heuristicType = HeuristicType.MISPLACED_TILES
        int[] goal = [1, 2, 3, 8, 0, 4, 7, 6, 5]
        int[] puzzle = [2, 8, 3, 1, 6, 4, 7, 0, 5]
        AStar solver = new AStar(goal)
        Node startNode = new Node(puzzleArray: puzzle)
        startNode.gCost = 0
        startNode.hCost = Heuristic.calculate(puzzle, goal, heuristicType)
        startNode.updatePathCost()
        solver.solve(heuristicType, startNode, 100)
    }


}

<%--
  Created by IntelliJ IDEA.
  User: gabriel
  Date: 11/20/12
  Time: 9:04 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Y.A.A.I.E. - Código Fonte</title>
</head>
<body>
<div id="page-body" role="main">
    <h1>Código Fonte</h1>
    <div id="content">
        <div class="text">
            <p>Esta aplicação foi desenvolvida em <g:link url="http://www.grails.org/" target="_blank">Grails</g:link>, um framework para desenvolvimento de aplicações web utilizando a linguagem dinâmica de programação <g:link url="http://groovy.codehaus.org/" target="_blank">Groovy</g:link>.</p>
        </div>
        <div class="text">
            <p>O único módulo até o momento, <g:link controller="eightPuzzle">8-Puzzle</g:link>, foi desenvolvido apenas em Groovy e compressado em um Java Archive (JAR) executável.</p>
            <p>Isso é possível pois o Groovy é desenvolido para a plataforma Java, sendo possível compilar os scripts Groovy para Java bytecodes e até mesmo mesclar os códigos.</p>
            <p>Para facilitar o processo de compilação, foi utilizada a ferramenta <g:link url="http://ant.apache.org/" target="_blank">Apache Ant</g:link>, um dos <g:link action="requirements">requerimentos</g:link> para compilar o projeto 8-Puzzle em seu computador.</p>
        </div>
        <div class="text">
            <p>Com isso, é possível fazer o <g:link url="https://bitbucket.org/gabriel24h/yaaie/downloads/ya8puzzle-1.0.jar">download</g:link> do JAR e executá-lo em sua máquina tendo apenas o Java Runtime Environment (JRE) instalado.</p>
            <p>Ou facilmente <g:link controller="eightPuzzle" action="solve">acoplá-lo</g:link> numa aplicação web.</p>
        </div>
        <div class="text">
            <p>Nesta seção, vamos abordar detalhes de como utilizar o projeto em seu computador, começando pelos seus <g:link action="requirements">requerimentos</g:link>.</p>
        </div>
    </div>
</div>
</body>
</html>
<div class="node-info">
    <g:if test="${node.gCost == 0}">
        <h2>Situação Inicial</h2>
    </g:if>
    <g:if test="${node.direction}">
        <p><strong>Movement:</strong> ${node.direction}</p>
    </g:if>
    <p><strong>g(n):</strong> ${node.gCost}</p>
    <p><strong>h(n):</strong> ${node.hCost}</p>
    <p><strong>f(n):</strong> ${node.pathCost}</p>
    <p><strong>Path Size:</strong> ${node.pathSize}</p>
</div>
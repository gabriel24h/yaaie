<div id="status" role="complementary" class="menu">
    <div class="nav">
        <h1><g:link name="home-index" controller="home" class="home"><strong>Y.A.A.I.E.</strong></g:link></h1>
        <ul>
            <li><g:link name="home-about" controller="home" action="about">Sobre</g:link></li>
        </ul>
    </div>
    <div class="nav">
        <h2><g:link name="sourceCode-index" controller="sourceCode"><strong>Código Fonte</strong></g:link></h2>
        <ul>
            <li ><g:link name="sourceCode-requirements" controller="sourceCode" action="requirements">Requerimentos</g:link></li>
            <li><g:link name="sourceCode-checkout" controller="sourceCode" action="checkout">Checkout / Download</g:link></li>
            <li><g:link name="sourceCode-build" controller="sourceCode" action="build">Compilação e Execução</g:link></li>
        </ul>
    </div>
    <div class="nav">
        <h2>Aplicações</h2>
        <g:link name="eightPuzzle-index" controller="eightPuzzle">8-Puzzle</g:link>
        <ul>
            <li><g:link name="eightPuzzle-program" controller="eightPuzzle" action="program">Programa</g:link></li>
            <li><g:link name="eightPuzzle-conclusions" controller="eightPuzzle" action="conclusions">Conclusões</g:link></li>
            <li><g:link name="eightPuzzle-solve" controller="eightPuzzle" action="solve">Solução</g:link></li>
            <li><g:link name="eightPuzzle-references" controller="eightPuzzle" action="references">Referências</g:link></li>
        </ul>
    </div>
</div>

<g:javascript>
    $('a[name="${params.controller}-${params.action}"]').addClass('nav a hover');
</g:javascript>

<hr>
<div class="solution-info">
    <h1>Informações Solução</h1>
    <p><strong>Situação Objetivo:</strong></p>
    <ya8puzzle:drawBoard puzzle="${solution.goal}"/>
    <hr>
    <p><strong>Tamanho Caminho Solução:</strong> ${solution.pathSize}</p>
    <p><strong>Nós Criados:</strong> ${solution.nodesCreated}</p>
    <p><strong>Nós Expandidos:</strong> ${solution.nodesExpanded}</p>
    <hr>
    <p><strong>Lista Open:</strong></p>
    <g:each var="node" in="${solution.openList.sort {it.pathCost}}">
        <ya8puzzle:drawSolutionInfoBoard puzzle="${node.puzzleArray}" node="${node}"/>
    </g:each>
    <hr>
    <p><strong>Lista Closed: </strong></p>
    <g:each var="node" in="${solution.closedList.sort {it.pathCost}}">
        <ya8puzzle:drawSolutionInfoBoard puzzle="${node.puzzleArray}" node="${node}"/>
    </g:each>
</div>